##Readme

#blog post
There is a blog post around this code here: https://tech.davidfield.co.uk/some-windows-automation-using-boxstarter/

#Running the Script
If our txt file is located here

    c:\files\installwin10.txt
From the Windows PC we want to run the install on the following command can be launched.

    START https://boxstarter.org/packages/ns/url?c:\files\installwin10.txt

If the txt was being hosted on a webpage you could also run all one one line the following.

    START https://boxstarter.org/packages/ns/url?http://mywebserver.me/installwin10.txt

The box starter URL is where the files needed to do the install are located and installed from, the location post url? is where the install file is located

This will

- Launch the default web browser
- Install a Boxstarter web plugin
- Ask for some permissions
- Setup the Boxstarter environment
- Run the script
